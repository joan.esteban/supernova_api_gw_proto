import connexion
import six

from swagger_server.models.eventpmholstersensor import EVENTPMHOLSTERSENSOR  # noqa: E501
from swagger_server.models.reqgetholsterstatus import REQGETHOLSTERSTATUS  # noqa: E501
from swagger_server.models.eventpmholstersensor_status import EVENTPMHOLSTERSENSORStatus  # noqa: F401,E501

from swagger_server import util


def req_get_holster_status(body=None):  # noqa: E501
    """Require verify user identity

     # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: EVENTPMHOLSTERSENSOR
    """
    if connexion.request.is_json:
        body = REQGETHOLSTERSTATUS.from_dict(connexion.request.get_json())  # noqa: E501
        allowed_values = [0,1,2]
        if body.holster_id not in allowed_values:
            return f"{{msg: 'Invalid holster_id {body.holster_id} must be one of {allowed_values}'}}",400
            

        if (body.holster_id==0):
            h1 = EVENTPMHOLSTERSENSORStatus(holster_id=1,status='busy')
            h2 = EVENTPMHOLSTERSENSORStatus(holster_id=2,status='free')
            return EVENTPMHOLSTERSENSOR(status=[h1,h2])
        else:
            return EVENTPMHOLSTERSENSOR(status=[EVENTPMHOLSTERSENSORStatus(holster_id=body.holster_id,status='busy')])

    return '{}'
