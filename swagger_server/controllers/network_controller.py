import connexion
import six

from swagger_server.models.eventnetworkwanstatus import EVENTNETWORKWANSTATUS  # noqa: E501
from swagger_server.models.eventnetworkwanstatus_ppp import EVENTNETWORKWANSTATUSPpp 
from swagger_server import util


current_config = EVENTNETWORKWANSTATUS()

def req_get_wan_data():  # noqa: E501
    """Set WAN data

     # noqa: E501


    :rtype: EVENTNETWORKWANSTATUS
    """
    return current_config


def req_set_wan_data(body=None):  # noqa: E501
    """Set WAN data

     # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: EVENTNETWORKWANSTATUS
    """
    global current_config
    if connexion.request.is_json:
        body = EVENTNETWORKWANSTATUS.from_dict(connexion.request.get_json())  # noqa: E501
        current_config = body
    return current_config
