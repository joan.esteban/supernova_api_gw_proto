import connexion
import six
from datetime import datetime

from swagger_server.models.eventuserauthenticated import EVENTUSERAUTHENTICATED  # noqa: E501
from swagger_server.models.requserauthenticatedata import REQUSERAUTHENTICATEDATA  # noqa: E501
from swagger_server import util


def event_req_authenticate(body=None):  # noqa: E501
    """Require verify user identity

     # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: EVENTUSERAUTHENTICATED
    """
    if connexion.request.is_json:
        body = REQUSERAUTHENTICATEDATA.from_dict(connexion.request.get_json())  # noqa: E501
        if len(body.username)==0:
            return f"{{msg: 'Invalid username {body.username} can't be empty'}}",400
            
        return EVENTUSERAUTHENTICATED(id=1234, 
                main_role="admin", 
                username=body.username,
                grants=["GRANT_WAN_READ", "GRANT_LAN_READ", "GRANT_CONFIGURATION_READ", "GRANT_CONFIGURATION_WRITE", "GRANT_BASIC_CHANGER_DATA_READ",
                         "GRANT_TELEMETRY_READ", "GRANT_TRANSACTION_LOG_READ", "GRANT_TRANSACTION_LOG_WRITE"], 
                authentication_date= datetime.now())

    return '{}'
