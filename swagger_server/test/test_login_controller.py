# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.eventuserauthenticated import EVENTUSERAUTHENTICATED  # noqa: E501
from swagger_server.models.requserauthenticatedata import REQUSERAUTHENTICATEDATA  # noqa: E501
from swagger_server.test import BaseTestCase


class TestLoginController(BaseTestCase):
    """LoginController integration test stubs"""

    def test_event_req_authenticate(self):
        """Test case for event_req_authenticate

        Require verify user identity
        """
        body = REQUSERAUTHENTICATEDATA()
        response = self.client.open(
            '/v1/wbx/login/command/REQ_USER_AUTHENTICATE',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
