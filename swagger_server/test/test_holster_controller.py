# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.eventpmholstersensor import EVENTPMHOLSTERSENSOR  # noqa: E501
from swagger_server.models.reqgetholsterstatus import REQGETHOLSTERSTATUS  # noqa: E501
from swagger_server.test import BaseTestCase


class TestHolsterController(BaseTestCase):
    """HolsterController integration test stubs"""

    def test_req_get_holster_status(self):
        """Test case for req_get_holster_status

        Require verify user identity
        """
        body = REQGETHOLSTERSTATUS()
        response = self.client.open(
            '/v1/wbx/power_manager/command/REQ_GET_HOLSTER_STATUS',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
