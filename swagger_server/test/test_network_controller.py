# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.eventnetworkwanstatus import EVENTNETWORKWANSTATUS  # noqa: E501
from swagger_server.test import BaseTestCase


class TestNetworkController(BaseTestCase):
    """NetworkController integration test stubs"""

    def test_req_get_wan_data(self):
        """Test case for req_get_wan_data

        Set WAN data
        """
        response = self.client.open(
            '/v1/wbx/network/command/REQ_GET_WAN_DATA',
            method='POST')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_req_set_wan_data(self):
        """Test case for req_set_wan_data

        Set WAN data
        """
        body = EVENTNETWORKWANSTATUS()
        response = self.client.open(
            '/v1/wbx/network/command/REQ_SET_WAN_DATA',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
